package wormholeservice

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
)

// WormholeService offers the functionalities from magic-wormhole in golang
type WormholeService struct {
}

// New creates a new WormholeService-object
func New() WormholeService {
	service := WormholeService{}
	return service
}

// Send a string trough magic-wormhole
func (service WormholeService) Send(text string) error {
	cmd := exec.Command("python", "-m", "wormhole", "send", "--text="+text)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		return errors.New("Wormhole send failed")
	}

	return nil
}

// Receive Text from another wormhole user
func (service WormholeService) Receive(text string) string {
	cmd := exec.Command("python", "-m", "wormhole", "receive", text)

	x, err := cmd.Output()
	if err == nil {
		fmt.Println("Received : " + string(x))
		return string(x)
	}

	fmt.Println(err.Error())
	return ""
}
